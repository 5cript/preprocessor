#ifndef PROJECT_HPP_INCLUDED
#define PROJECT_HPP_INCLUDED

#include <vector>
#include <string>
#include <boost/filesystem.hpp>

#include "ProjectForward.hpp"
#include "SeeScriptSource.hpp"

namespace SeeScript {
	struct invalid_directory_exception : public std::exception {
		const char* what() const throw();
	};
	struct file_not_found_exception : public std::exception {
		const char* what() const throw();
	};

	class SeeScriptProject 
	{
	private:
		std::string systemHeaderDirectory_;
		std::string systemLibraryDirectory_;
		std::vector <SourceFile> sources_;
		std::vector <std::string> compilerIncludeDirectories_;
		std::vector <std::string> linkerLibraryDirectories_;
	public:
		SeeScriptProject(std::string systemHeaderDirectory, std::string systemLibraryDirectory);
		void AddIncludeDirectory(std::string directoryName);
		void AddLibraryDirectory(std::string directoryName);

		// only add C files, no header files
		void AddSourceFile(std::string const& fileName);
		void AddSource(std::string const& dataString);
		void AddSource(std::vector <char> const& data);

		std::string findSource(std::string const& fileName, std::string const& relativeSearchDirectory = {}, bool systemHeader = false) const;
		std::string findLibrary(std::string const& fileName, bool systemLibrary = false) const;
	};
}

#endif