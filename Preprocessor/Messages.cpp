#include "Messages.hpp"

#ifndef _MSC_VER
#	define NOEXCEPT noexcept
#else
#	define NOEXCEPT throw()
#endif

namespace SeeScript { namespace Preprocessor {
	const char* Message::what() const NOEXCEPT
	{
		return "message";
	}
	std::string Message::message() const
	{
		return message_;
	}
	unsigned int Message::code() const
	{
		return code_;
	}
	int Message::line() const
	{
		return line_;
	}
	std::string Message::file() const
	{
		return fileName_;
	}

	const char* Warning::what() const NOEXCEPT
	{
		return "warning";
	}

	const char* Error::what() const NOEXCEPT
	{
		return "error";
	}
} // Preprocessor
} // SeeScript
