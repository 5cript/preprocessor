#ifndef PREPROCESSOR_TOKENS_H_INCLUDED
#define PREPROCESSOR_TOKENS_H_INCLUDED

#include <string>
#include <vector>

namespace SeeScript { namespace Preprocessor { namespace Tokenizer {
	// Base class for all tokens
	enum class TokenID : unsigned
	{
		OPERATOR,
		IDENTIFIER,
		NUMERAL,
		STRING_LITERAL,
		CHAR_LITERAL,
		HEADER_STRING_LITERAL,
		GENERIC
	};

	class Token
	{
	public:
		std::string toString() const;
		operator std::string() const;
		unsigned int size() const;
		bool operator==(std::string const& str);
		bool operator==(const char* cstr);
		bool operator==(char const c);

		bool operator!=(std::string const& str);
		bool operator!=(const char* cstr);
		bool operator!=(char const c);

		Token& operator=(Token const&) = default;
		Token(Token const&) = default;

		virtual TokenID getID() const;
		virtual Token* clone() const;

		// STATIC VIRTUAL
		// static Token* applicable(std::string::const_iterator& begin, std::string::const_iterator end);

		virtual ~Token() = default;
		Token(std::string str) : tokenRepresentation(std::move(str)) {}
		Token() : tokenRepresentation{} {}
	protected:
		std::string tokenRepresentation;
	};

	class OperatorToken : public Token
	{
	public:
		TokenID getID() const override;
		OperatorToken* clone() const override;
		static OperatorToken* applicable(std::string::const_iterator& begin, std::string::const_iterator end);
		OperatorToken(std::string str) : Token(std::move(str)) {}
	};

	class IdentifierToken : public Token
	{
	public:
		TokenID getID() const override;
		IdentifierToken* clone() const override;
		static IdentifierToken* applicable(std::string::const_iterator& begin, std::string::const_iterator end);
		IdentifierToken(std::string str) : Token(std::move(str)) {}
	};

	class NumericToken : public Token
	{
	private:
		bool negative;
	public:
		TokenID getID() const override;
		NumericToken* clone() const override;
		bool isNegative() const;
		void setNegative(bool neg);
		static NumericToken* applicable(std::string::const_iterator& begin, std::string::const_iterator end);
		NumericToken(std::string str, bool negative = false) : Token(std::move(str)), negative(negative) {}
	};

	class LiteralToken : public Token
	{
	public:
		LiteralToken(std::string str) : Token(std::move(str)) {}
	};

	class StringLiteralToken : public LiteralToken
	{
	public:
		TokenID getID() const override;
		StringLiteralToken* clone() const override;
		static StringLiteralToken* applicable(std::string::const_iterator& begin, std::string::const_iterator end);
		StringLiteralToken(std::string str) : LiteralToken(std::move(str)) {}
	};

	class CharLiteralToken : public LiteralToken
	{
	public:
		TokenID getID() const override;
		CharLiteralToken* clone() const override;
		int toNumber() const;
		static CharLiteralToken* applicable(std::string::const_iterator& begin, std::string::const_iterator end);
		CharLiteralToken(std::string str) : LiteralToken(std::move(str)) {}
	};

	class HeaderStringToken : public LiteralToken
	{
	private:
		bool systemHeader_;
	public:
		TokenID getID() const override;
		HeaderStringToken* clone() const override;
		static HeaderStringToken* applicable(std::string::const_iterator& begin, std::string::const_iterator end);
		HeaderStringToken(std::string str, bool systemHeader)
			: LiteralToken(std::move(str))
			, systemHeader_(systemHeader){}

		bool isSystemHeader();
	};

	class GenericToken : public Token
	{
	public:
		TokenID getID() const override;
		GenericToken* clone() const override;
		GenericToken(std::string str) : Token(std::move(str)) {}
	};

	template <typename T, typename ... List>
	struct TokenSelector
	{
		static Token* getApplicable(std::string::const_iterator& begin, std::string::const_iterator end)
		{
			Token* tok = T::applicable(begin, end);
			if (tok)
				return tok;
			else
				return TokenSelector<List...>::getApplicable(begin, end);
		}
	};

	template <>
	struct TokenSelector <void>
	{
		static Token* getApplicable(std::string::const_iterator& begin, std::string::const_iterator end)
		{
			return nullptr;
		}
	};

	inline Token* extractToken(std::string::const_iterator& begin, std::string::const_iterator end)
	{
		return TokenSelector <OperatorToken, IdentifierToken, NumericToken, CharLiteralToken,
			StringLiteralToken, void>::getApplicable(begin, end);
	}

	bool isEscapeSequence(std::string::const_iterator& begin, std::string::const_iterator end);
	// returns nullptr if invalid resulting token
	Token* tokenConcatentator(std::string tokenString_lhs, std::string tokenString_rhs);
} // Tokenizer
} // Preprocessor
} // SeeScript


#endif // PREPROCESSOR_TOKENS_H_INCLUDED
