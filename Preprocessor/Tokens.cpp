#include "Tokens.hpp"

#include <cctype>
#include <boost/regex.hpp>

const std::string operatorTokens[] = {
	"...", "<<=", ">>=", "!=", "%=", "##",
	"&&", "&=", "*=", "++", "+=", "--", "-=", "->", "/=", "<<", "<=", "==", ">=",
	">>", "^=", "|=", "||", "!", "~", "%", "&", "(", ")", "*", "+", ",", "-", ".",
	"/", ":", ";", "<", "=", ">", "?", "[", "]", "^", "{", "|", "}", "#"
};

namespace SeeScript { namespace Preprocessor { namespace Tokenizer {
	std::string Token::toString() const
	{
		return tokenRepresentation;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	Token::operator std::string() const
	{
		return toString();
	}
//---------------------------------------------------------------------------------------------------------------------------------
	unsigned int Token::size() const
	{
		return tokenRepresentation.length();
	}
//---------------------------------------------------------------------------------------------------------------------------------
	bool Token::operator==(std::string const& str)
	{
		return tokenRepresentation == str;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	bool Token::operator==(const char* cstr)
	{
		return tokenRepresentation == cstr;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	bool Token::operator==(char const c)
	{
		if (tokenRepresentation.size() != 1)
			return false;
		return tokenRepresentation[0] == c;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	bool Token::operator!=(std::string const& str)
	{
		return tokenRepresentation != str;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	bool Token::operator!=(const char* cstr)
	{
		return tokenRepresentation != cstr;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	bool Token::operator!=(char const c)
	{
		if (tokenRepresentation.size() != 1)
			return true;
		return tokenRepresentation[0] != c;
	}
//#################################################################################################################################
	TokenID Token::getID() const
	{
		return TokenID::GENERIC;
	}
//---------------------------------------------------------------------------------------------------------------------------------;
	TokenID OperatorToken::getID() const
	{
		return TokenID::OPERATOR;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	TokenID IdentifierToken::getID() const
	{
		return TokenID::IDENTIFIER;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	TokenID NumericToken::getID() const
	{
		return TokenID::NUMERAL;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	TokenID StringLiteralToken::getID() const
	{
		return TokenID::STRING_LITERAL;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	TokenID CharLiteralToken::getID() const
	{
		return TokenID::CHAR_LITERAL;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	TokenID HeaderStringToken::getID() const
	{
		return TokenID::HEADER_STRING_LITERAL;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	TokenID GenericToken::getID() const
	{
		return TokenID::GENERIC;
	}
//#################################################################################################################################
	Token* Token::clone() const
	{
		return new Token(*this);
	}
//---------------------------------------------------------------------------------------------------------------------------------;
	OperatorToken* OperatorToken::clone() const
	{
		return new OperatorToken(*this);
	}
//---------------------------------------------------------------------------------------------------------------------------------
	IdentifierToken* IdentifierToken::clone() const
	{
		return new IdentifierToken(*this);
	}
//---------------------------------------------------------------------------------------------------------------------------------
	NumericToken* NumericToken::clone() const
	{
		return new NumericToken(*this);
	}
//---------------------------------------------------------------------------------------------------------------------------------
	StringLiteralToken* StringLiteralToken::clone() const
	{
		return new StringLiteralToken(*this);
	}
//---------------------------------------------------------------------------------------------------------------------------------
	CharLiteralToken* CharLiteralToken::clone() const
	{
		return new CharLiteralToken(*this);
	}
//---------------------------------------------------------------------------------------------------------------------------------
	HeaderStringToken* HeaderStringToken::clone() const
	{
		return new HeaderStringToken(*this);
	}
//---------------------------------------------------------------------------------------------------------------------------------
	GenericToken* GenericToken::clone() const
	{
		return new GenericToken(*this);
	}
//#################################################################################################################################
	int CharLiteralToken::toNumber() const
	{
		int res = 0;
		int c = 0;
		for (auto i = std::begin(tokenRepresentation) + 1; i < std::end(tokenRepresentation) - 1; ++i)
		{
			res |= *i << ((c++)*8);
		}
		return res;
	}
//#################################################################################################################################
	bool NumericToken::isNegative() const
	{
		return negative;
	}
	void NumericToken::setNegative(bool neg)
	{
		negative = neg;
	}
//#################################################################################################################################
	OperatorToken* OperatorToken::applicable(std::string::const_iterator& begin, std::string::const_iterator end)
	{
		if (begin == end)
			return {};
		auto cur = begin;
		for (auto const& i : operatorTokens)
		{
			bool fullmatch = true;
			cur = begin;
			auto j = std::begin(i);
			for (; j != std::end(i) && cur != end; ++j)
			{
				if (*cur != *j)
				{
					fullmatch = false;
					break;
				}
				++cur;
			}
			if (cur == end && j != std::end(i))
				continue;
			if (fullmatch)
			{
				begin = cur - 1;
				return new OperatorToken{i};
			}
		}
		return nullptr;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	IdentifierToken* IdentifierToken::applicable(std::string::const_iterator& begin, std::string::const_iterator end)
	{
		auto alt_begin = begin;
		if (*begin==isdigit(*begin))
			return nullptr;
		if (*begin!='_' && !isalpha(*begin))
			return nullptr;
		for (; begin != end && ( isalnum(*begin) || *begin=='_' ); ++begin);
		if (alt_begin != begin)
			--begin;
		return new IdentifierToken {std::string{alt_begin, begin + 1}};
	}
//---------------------------------------------------------------------------------------------------------------------------------
	NumericToken* NumericToken::applicable(std::string::const_iterator& begin, std::string::const_iterator end)
	{
		boost::regex regex {"\\.?\\d(?:(?:[EepP][+-])|(?:\\w|\\d|\\.))*"};
		auto temp = std::string{begin, end};

		boost::match_results<std::string::const_iterator> what;
		auto temp_iter = begin;
		if(!boost::regex_search(temp_iter, end, what, regex))
		{
			// the input so far could not possibly be valid so reject it:
			return nullptr;
		}
		if (what.position() != 0)
			return nullptr;
		auto res = static_cast <std::string> (*std::begin(what));
		begin += (res.length() - 1);
		return new NumericToken {NumericToken{res}};
	}
//---------------------------------------------------------------------------------------------------------------------------------
	CharLiteralToken* CharLiteralToken::applicable(std::string::const_iterator& begin, std::string::const_iterator end)
	{
#define _FAIL {begin=alt_begin; return nullptr;}
		auto alt_begin = begin;
		if (*begin=='L') begin++;
		if (*begin!='\'') _FAIL
		else begin++;
		auto before = begin;
		for (; begin != end && ( !(*begin=='\'' || *begin=='\\') || isEscapeSequence(begin, end) ); ++begin);
		if (begin == end) _FAIL
		if (before == begin) _FAIL
		if (*begin!='\'') _FAIL
		return new CharLiteralToken {std::string{alt_begin, begin + 1}};
#undef _FAIL
	}
//---------------------------------------------------------------------------------------------------------------------------------
	StringLiteralToken* StringLiteralToken::applicable(std::string::const_iterator& begin, std::string::const_iterator end)
	{
#define _FAIL {begin=alt_begin; return nullptr;}
		auto alt_begin = begin;
		if (*begin=='L') begin++;
		if (*begin!='"') _FAIL
		else begin++;
		for (; begin != end && ( (*begin!='"' && *begin!='\\') || isEscapeSequence(begin, end) ); ++begin);
		if (begin == end) _FAIL
		if (*begin!='"') _FAIL
		return new StringLiteralToken {std::string{alt_begin, begin + 1}};
#undef _FAIL
	}
//---------------------------------------------------------------------------------------------------------------------------------
	HeaderStringToken* HeaderStringToken::applicable(std::string::const_iterator& begin, std::string::const_iterator end)
	{
#define _FAIL {begin=alt_begin; return nullptr;}
		bool angles = true; // < or " ?

		auto alt_begin = begin;
		if (*begin=='L') begin++;
		if (*begin=='<')
		{
			angles = true;
		}
		else if (*begin=='"')
		{
			angles = false;
		}
		else _FAIL
		begin++;
		for (; begin != end && ((angles && *begin!='>') || (!angles && *begin!='"')); ++begin);
		if (begin == end) _FAIL
		if (angles && *begin!='>') _FAIL
		if (!angles && *begin!='"') _FAIL
		return new HeaderStringToken {std::string{alt_begin + 1, begin}, angles}; // omit <> or ""
#undef _FAIL
	}
//#################################################################################################################################
	bool isEscapeSequence(std::string::const_iterator& begin, std::string::const_iterator end)
	{
		//\\\\(?:[\"'\\?\\\\abfnrtv]|(?:[0-7][0-7]?[0-7]?)|(x\\w+))
		boost::regex regex {"\\\\(?:[\"'\\?\\\\abfnrtv]|(?:[0-7][0-7]?[0-7]?)|(x\\w+))"};
		boost::match_results<std::string::const_iterator> what;
		if(!boost::regex_search(begin, end, what, regex))
		{
			return false;
		}
		if (what.position() != 0)
			return false;
		begin += what.length() - 1;
		return true;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	Token* tokenConcatentator(std::string tokenString_lhs, std::string tokenString_rhs)
	{
		std::string resulting_token = tokenString_lhs + tokenString_rhs;
		auto beg = resulting_token.cbegin();
		auto tok = extractToken(beg, resulting_token.cend());
		if (tok == nullptr)
			return nullptr;
		if (tok->toString().length() != resulting_token.length())
			return nullptr;
		else
			return tok;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	bool HeaderStringToken::isSystemHeader()
	{
		return systemHeader_;
	}
//---------------------------------------------------------------------------------------------------------------------------------
} // Tokenizer
} // Preprocessor
} // SeeScript
