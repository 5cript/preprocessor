#ifndef PREPROCESSOR_CONSTANTS_HPP_INCLUDED
#define PREPROCESSOR_CONSTANTS_HPP_INCLUDED

namespace SeeScript { namespace Preprocessor {

// I maybe should drop support ...
/*
	const std::pair <std::string, std::string> trigraphs[] = {
#ifdef _MSC_VER
		{ "??/", "\\" },
		{ "??'", "^" },
		{ "??(", "[" },
		{ "??)", "]" },
		{ "??!", "|" },
		{ "??<", "{" },
		{ "??>", "}" },
		{ "??-", "~" }

#else
#   // "trigraphs are problematic when compiled with gcc"
#endif
	};
*/

	const std::pair <std::string, std::string> digraphs[] = {
		{ "<%", "{" },
		{ "%>", "}" },
		{ "<:", "[" },
		{ ":>", "]" },
		{ "%:", "#" }
	};

	const std::string INCLUDE_DIRECTIVE = "include";
	const std::string DEFINE_DIRECTIVE = "define";
	const std::string UNDEFINE_DIRECTIVE = "undef";
	const std::string ERROR_DIRECTIVE = "error";
	const std::string IFDEF_DIRECTIVE = "ifdef";
	const std::string IFNDEF_DIRECTIVE = "ifndef";
	const std::string IF_DIRECTIVE = "if";
	const std::string ENDIF_DIRECTIVE = "endif";
	const std::string ELSE_DIRECTIVE = "else";
	const std::string ELIF_DIRECTIVE = "elif";

} // SeeScript
} // Preprocessor

#endif // PREPROCESSOR_CONSTANTS_HPP_INCLUDED
