#ifndef PREPROCESSOR_MATHEMATICS_HPP
#define PREPROCESSOR_MATHEMATICS_HPP

#include "Tokens.hpp"

#include <vector>
#include <string>
#include <memory>
#include <exception>

namespace SeeScript { namespace Preprocessor { namespace Mathematics {
	typedef long long NumeralType;

	enum class MathError
	{
		NO_ERROR,
		UNMATCHED_PARANTHESIS,
		INVALID_OPERATOR,
		UNEXPECTED_TOKEN,
		MISSING_OPERANDS,
		NOT_AN_INTEGER,
		TOO_BIG,
		INCORRECT_VALUE_AMOUNT
	};

	struct MathErrorInfo
	{
		MathError code;
		std::string additionalInformation;

		MathErrorInfo(MathError code = MathError::NO_ERROR, std::string additionalInformation = "")
			: code (code)
			, additionalInformation(std::move(additionalInformation))
		{}
	};

	struct MathematicalError : public std::exception
	{
	public:
		MathematicalError(MathErrorInfo info) : info(info) {}
		const char* what() const throw() override;
		MathErrorInfo getInfo() const;
	private:
		MathErrorInfo info;
	};

	struct ReversePolishNotation
	{
		std::vector <std::shared_ptr <Tokenizer::Token>> output;
	};

	ReversePolishNotation ShuntingYard(std::vector <std::shared_ptr <Tokenizer::Token>> const& tokens);
	NumeralType EvaluateReversePolish(ReversePolishNotation const& rpn);

} // Mathematics
} // Preprocessor
} // SeeScript

#endif // PREPROCESSOR_MATHEMATICS_HPP