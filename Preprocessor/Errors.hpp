#ifndef PREPROCESSOR_ERRORS_HPP_INCLUDED
#define PREPROCESSOR_ERRORS_HPP_INCLUDED

#include "Messages.hpp"

#ifndef _MSC_VER
#	define NOEXCEPT noexcept
#else
#	define NOEXCEPT throw()
#endif

namespace SeeScript { namespace Preprocessor {
	REGISTER_ERROR(CriticalIPE, "IPE: an internal error occured - %s", 1000);
	REGISTER_ERROR(CarryThrough, "Can you see me? I should have been caught!", 1001);


	REGISTER_ERROR(IncompleteStringLiteral, "missing \" character", 1);		
	REGISTER_ERROR(IncompleteCharLiteral, "missing ' character", 2);		
	REGISTER_ERROR(InvalidStringLiteral, "invaliad string literal", 3);		
	REGISTER_ERROR(InvalidCharLiteral, "invalid character literal", 4);		
	REGISTER_ERROR(IncludeErroneous, "#include expects \"FILENAME\" or <FILENAME>", 5);		
	REGISTER_ERROR(IncludeFileNotFound, "cannot find or open file specified", 6);			
	REGISTER_ERROR(IncludeNested, "#include nested too deeply", 7);		
	REGISTER_ERROR(UnknownDirective, "invalid preprocessing directive", 8);
	REGISTER_ERROR(MissingMacroName, "no macro name given in #%s directive", 9);
	REGISTER_ERROR(MissingMacroName2, "no macro name given in #define directive (second check)", 10);
	REGISTER_ERROR(UnallowedParanthesis, "'(' may not appear in macro parameter list", 11);
	REGISTER_ERROR(MissingParanthesis, "missing ')' in macro parameter list", 11);
	REGISTER_ERROR(InvalidTokenInParameters, "\"%s\" may not appear in macro parameter list", 12);
	REGISTER_ERROR(MustBeCommaSeparated, "macro parameters must be comma-separated", 13);
	REGISTER_ERROR(DuplicateParameter, "duplicate macro parameter", 14);
	REGISTER_ERROR(UnterminatedConditional, "unterminated #%s", 15);
	REGISTER_ERROR(DanglingEndif, "#endif without #if", 16);
	REGISTER_ERROR(MissingExpression, "#%s with no expression", 17);
	REGISTER_ERROR(DefinedOperatorRequiresIdentifier, "operator \"defined\" requires an identifier", 18);
	REGISTER_ERROR(MissingOperand, "operator '%s' has no operand", 19);
	REGISTER_ERROR(IfMathError, "evalutation of expression failed, because: %s", 20);
	REGISTER_ERROR(TooMuchActualParameters, "macro \"%s\" passed %s arguments, but takes just %s", 21);
	REGISTER_ERROR(NotEnoughActualParameters, "macro \"%s\" requires %s arguments, but only %s given", 22);
	REGISTER_ERROR(UnterminatedArgumentList, "unterminated argument list invoking macro \"%s\"", 23);
	REGISTER_ERROR(TokenPastingOperatorMisplaced, "## cannot appear at either end of a macro expansion", 24);
	REGISTER_ERROR(InvalidTokenCombination, "the concatination of \"%s\" and \"%s\" does not result in a valid preprocessor token", 25);
	REGISTER_ERROR(MissingParanthesisAfterDefined, "missing ')' after \"defined\"", 26);
	REGISTER_ERROR(DanglingElif, "#elif without #if", 27);
	REGISTER_ERROR(StrayPPOP, "stray '%s' in Program", 28);
	REGISTER_ERROR(StringizeNonParam, "'#' is not followed by a macro parameter", 29);
} // Preprocessor
} // SeeScript

#undef NOEXCEPT

#endif // PREPROCESSOR_ERRORS_HPP_INCLUDED