#ifndef PREPROCESSOR_WARNINGS_HPP_INCLUDED
#define PREPROCESSOR_WARNINGS_HPP_INCLUDED

#include "Messages.hpp"

#ifndef _MSC_VER
#	define NOEXCEPT noexcept
#else
#	define NOEXCEPT throw()
#endif

namespace SeeScript { namespace Preprocessor {
	REGISTER_WARNING(RedundandTokens, "extra tokens at end of #%s directive", 1);		
	REGISTER_WARNING(Redefinition, "\"%s\" redefined", 2);
	REGISTER_WARNING(VariadicMacroVA_ARGS, "__VA_ARGS__ can only appear in the expansion of a C99 variadic macro", 3);
} // Preprocessor
} // SeeScript

#undef NOEXCEPT

#endif // PREPROCESSOR_WARNINGS_HPP_INCLUDED