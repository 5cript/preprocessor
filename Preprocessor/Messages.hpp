#ifndef PREPREOCESSOR_MESSAGES_H_INCLUDED
#define PREPREOCESSOR_MESSAGES_H_INCLUDED

#include <exception>
#include <string>
#include <vector>

#ifdef _MSC_VER
#	define NOEXCEPT throw()
#else
#	define NOEXCEPT noexcept
#endif // _MSC_VER

namespace SeeScript { namespace Preprocessor {
	// not meant to be thrown, but provides same interface
	class Message : public std::exception
	{
	public:
		Message(std::string message, unsigned int code, int line, std::string fileName,
				std::vector <std::string> const& additional_parms)
			: message_
			{
				[&]() /*noexcept*/ -> std::string
				{
					std::string::size_type pos = 0;
					for (auto const& i : additional_parms)
					{
						pos = message.find("%s", pos);
						if (pos == std::string::npos)
							break;
						else
							message.replace(pos, 2, i);
					}
					return message;
				}()
			}
			, code_{code}
			, line_{line}
			, fileName_{std::move(fileName)}
		{

		}

		virtual const char* what() const NOEXCEPT override;

		std::string message() const;
		unsigned int code() const;
		int line() const;
		std::string file() const;
	protected:
		const std::string message_;
		const unsigned int code_;
		const int line_;
		const std::string fileName_;
	};

	class Warning : public Message
	{
	public:
		Warning(std::string message, unsigned int code, int line,
				std::string fileName, std::vector <std::string> const& additional_parms = std::vector <std::string>{})  // type in initialiser prevents bogus nullptr exception
			: Message(std::move(message), code, line, std::move(fileName), additional_parms)
		{
		}

		virtual const char* what() const NOEXCEPT override; /* noexcept */
	};

	class Error : public Message
	{
	public:
		Error(std::string message, unsigned int code, int line,
			  std::string fileName, std::vector <std::string> const& additional_parms = std::vector <std::string>{}) // type in initialiser prevents bogus nullptr exception
			: Message(std::move(message), code, line, std::move(fileName), additional_parms)
		{
		}

		virtual const char* what() const NOEXCEPT override; /* noexcept */
	};

#define REGISTER_WARNING(NAME, MESSAGE, CODE)			\
	class NAME : public Warning							\
	{													\
	public:												\
		NAME(int line, std::string fileName,			\
	std::vector <std::string> const& additional_parms	\
	= std::vector <std::string>{})						\
			: Warning(MESSAGE, CODE, line,				\
			std::move(fileName), additional_parms)		\
		{												\
		}												\
														\
		virtual const char* what() const NOEXCEPT override \
		{												\
			return Warning::what();						\
		}												\
	}

#define REGISTER_ERROR(NAME, MESSAGE, CODE)				\
	class NAME : public Error							\
	{													\
	public:												\
		NAME(int line, std::string fileName,			\
	std::vector <std::string> const& additional_parms	\
	= std::vector <std::string>{})						\
			: Error(MESSAGE, CODE, line,				\
			std::move(fileName), additional_parms)		\
		{												\
		}												\
														\
		virtual const char* what() const NOEXCEPT override \
		{												\
			return Error::what();						\
		}												\
	}
} // Preprocessor
} // SeeScript

#undef NOEXCEPT

#endif // PREPREOCESSOR_MESSAGES_H_INCLUDED
