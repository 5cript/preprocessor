#ifndef PREPROCESSOR_HELPERS_H_INCLUDED
#define PREPROCESSOR_HELPERS_H_INCLUDED

#include <string>
#include <vector>
#include <deque>
#include <type_traits>

namespace SeeScript { namespace Preprocessor {

	inline bool isWhitespace(std::string::value_type character)
	{
		return (character <= 32);
	}

	template <typename T, template <typename T, typename = std::allocator <T>> class ContainerT = std::vector>
	void removeFromList(ContainerT<T>& list, std::deque<typename ContainerT<T>::size_type>& rem_list)
	{
		// decltype workaround for VC12 (correct: decltype(rem_list)::value_type)
		for (typename std::remove_reference<decltype(rem_list[0])>::type i = 0; !rem_list.empty(); ++i) {
			auto iter = std::begin(list);
			std::advance(iter, rem_list.front() - i);
			list.erase(iter);
			rem_list.pop_front();
		}
	}

	inline bool isStringLiteral(std::string::value_type character)
	{
		return (character == '"');
	}

	inline bool isCharLiteral(std::string::value_type character)
	{
		return (character == '\'');
	}

	inline bool isMulticharLiteral(std::string::value_type character)
	{
		return isStringLiteral(character) || isCharLiteral(character);
	}

	// iterate over the current line until the predicate matches a character and evaluates to true
	template <typename StringT, typename PredicateT> // Predicate is callable
	bool skipUntil(typename StringT::iterator& begin,
				   typename StringT::iterator end,
				   PredicateT predicate)
	{
		for (; begin != end; ++begin)
		{
			if (predicate(*begin))
			{
				return true;
			}
		}
		return false;
	}

} // Preprocessor
} // SeeScript

#endif // PREPROCESSOR_HELPERS_H_INCLUDED
