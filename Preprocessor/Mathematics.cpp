#include "Mathematics.hpp"

#include <stack>
#include <functional>
#include <boost/lexical_cast.hpp>

namespace SeeScript { namespace Preprocessor { namespace Mathematics {
	using namespace Tokenizer;
//---------------------------------------------------------------------------------------------------------------------------------
	struct Operator
	{
		std::string symbol;
		bool left_associative;
		unsigned int arity;
		int precedence;	// lower = higher precedence
		std::function <NumeralType(NumeralType, NumeralType, NumeralType)> operation;

		Operator(std::string symbol, bool left_associative = false, unsigned int arity = 0, int precedence = 0, decltype(operation) operation = [](NumeralType, NumeralType, NumeralType) -> NumeralType {return 0;})
			: symbol(std::move(symbol))
			, left_associative(left_associative)
			, arity(arity)
			, precedence(precedence)
			, operation(operation)
		{}

		bool operator==(Operator const& rhs) const
		{
			return rhs.symbol == symbol;
		}

		NumeralType operator()(NumeralType first, NumeralType second = 0, NumeralType third = 0) const
		{
			return operation(first, second, third);
		}
	};

	// odered by precedence
	const Operator operators[] = 
	{
		{"!", false, 2, 1 , [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return !second; } },
		{"~", false, 2, 1 , [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return ~second; }},
		{"*", true, 2, 2  , [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return first * second; }},
		{"/", true, 2, 2  , [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return first / second; }},
		{"%", true, 2, 2  , [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return first % second; }},
		{"+", true, 2, 3  , [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return first + second; }},
		{"-", true, 2, 3  , [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return first - second; }},
		{"<<", true, 2, 4 , [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return first << second; }},
		{">>", true, 2, 4 , [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return first >> second; }},
		{"<", true, 2, 5  , [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return first < second; }},
		{"<=", true, 2, 5 , [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return first <= second; }},
		{">", true, 2, 6  , [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return first > second; }},
		{">=", true, 2, 6 , [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return first >= second; }},
		{"==", true, 2, 7 , [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return first == second; }},
		{"!=", true, 2, 7 , [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return first != second; }},
		{"&", true, 2, 8  , [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return first & second; }},
		{"^", true, 2, 9  , [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return first ^ second; }},
		{"|", true, 2, 10 , [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return first | second; }},
		{"&&", true, 2, 11, [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return first && second; }},
		{"||", true, 2, 12, [](NumeralType first, NumeralType second, NumeralType) -> NumeralType { return first || second; }}
	};
//---------------------------------------------------------------------------------------------------------------------------------
	ReversePolishNotation ShuntingYard(std::vector <std::shared_ptr <Tokenizer::Token>> const& tokens)
	{
		ReversePolishNotation rpn;
		std::deque <std::shared_ptr <Token>> operatorStack;

		for (auto const& i : tokens)
		{
			switch (i->getID())
			{
				case (TokenID::NUMERAL):
				{
					rpn.output.push_back(i);
					break;
				}
				case (TokenID::OPERATOR):
				{
					if (*i == "(")		// opening paranthesis
					{
						operatorStack.push_back(i);
						break;	
					} 
					else if (*i == ")")	// closing paranthesis
					{
						bool found = false;
						while (!operatorStack.empty())
						{
							auto op = operatorStack.back();
							operatorStack.pop_back();
							if (*op != "(")
							{
								rpn.output.push_back(op);
							}
							else
							{
								found = true;
								break;
							}
						};
						if (!found)
						{
							throw MathematicalError { { MathError::UNMATCHED_PARANTHESIS } };
						}
					}
					else	// operator
					{
						while (!operatorStack.empty())
						{
							auto const* o1_iter = std::find(std::begin(operators), std::end(operators), Operator(*i));
							auto const* o2_iter = std::find(std::begin(operators), std::end(operators), Operator(*operatorStack.back()));	
							if (o2_iter == std::end(operators))
							{
								break; // is not an operator
							}
							if (o1_iter == std::end(operators))
							{
								throw MathematicalError { { MathError::INVALID_OPERATOR, *i } };
							}
							auto o2 = *o2_iter;
							auto o1 = *o1_iter;
							if (	o2.left_associative && o2.precedence == o1.precedence
								||	o2.precedence < o1.precedence)
							{
								rpn.output.push_back(operatorStack.back());
								operatorStack.pop_back();
							}
							else
								break;
						}
						operatorStack.push_back(i);
					}
					break;
				}
				default:
				{
					throw MathematicalError { { MathError::UNEXPECTED_TOKEN } };
				}
			}
		}

		while (!operatorStack.empty())
		{
			auto op = operatorStack.back();
			if (*op == ")" || *op == "(")
			{
				throw MathematicalError { { MathError::UNMATCHED_PARANTHESIS } };
			}
			else
			{
				rpn.output.push_back(op);
				operatorStack.pop_back();
			}
		}

		return rpn;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	NumeralType EvaluateReversePolish(ReversePolishNotation const& rpn)
	{
		std::stack <NumeralType> numeralStack;

		auto tokenToInt = [](NumericToken token) -> NumeralType 
		{
			try
			{
				auto n = std::stoll (token.toString(), nullptr, 0);
				if (token.isNegative())
					n *= -1;
				return n;
			}
			catch (std::invalid_argument&)
			{
				throw MathematicalError { { MathError::NOT_AN_INTEGER } };
			}
			catch (std::out_of_range&)
			{
				throw MathematicalError { { MathError::TOO_BIG } };
			}
		};

		for (auto const& i : rpn.output)
		{
			if (i->getID() == TokenID::NUMERAL)
			{
				numeralStack.push(tokenToInt(*static_cast <NumericToken*> (i.get())));
			}
			else
			{
				auto const* op = std::find(std::begin(operators), std::end(operators), Operator(*i));
				if (op == std::end(operators))
				{
					throw MathematicalError { { MathError::INVALID_OPERATOR, *i } };
				}
				else
				{
					if (numeralStack.size() < op->arity)
						throw MathematicalError { { MathError::MISSING_OPERANDS } };
					else 
					{
						NumeralType n[3] = {0, 0, 0};
						for (int i = 0; i != op->arity; ++i)
						{
							n[i] = numeralStack.top();
							numeralStack.pop();
						}
						numeralStack.push(op->operator()(n[1], n[0], n[2]));
					}
				}
			}
		}
		if (numeralStack.size() != 1)
			throw MathematicalError { { MathError::INCORRECT_VALUE_AMOUNT } };
		return numeralStack.top();
	}
//---------------------------------------------------------------------------------------------------------------------------------
	const char* MathematicalError::what() const throw()
	{
		return "Mathematical Error";
	}
	MathErrorInfo MathematicalError::getInfo() const
	{
		return info;
	}
//---------------------------------------------------------------------------------------------------------------------------------
} // SeeScript
} // Preprocessor
} // Mathematics