#ifndef SEESCRIPTSOURCE_HPP_INCLUDED
#define SEESCRIPTSOURCE_HPP_INCLUDED

#include <string>
#include <vector>

#include "ProjectForward.hpp"

namespace SeeScript { 
	class SourceFile {
	private:
		SeeScriptProject const* const project_;
		std::string fileName_; // optional
		std::vector <char> content_;
		bool systemHeader_;

		void normalize(); // change all line endings to unix file format (whenever the content is loaded)
	public:
		SourceFile(SeeScriptProject const* const project, std::string const& fileName = std::string{}, std::string const& relativeSearchDirectory = std::string{}, bool systemHeader = false);

		void loadFromFile(std::string const& fileName, std::string const& relativeSearchDirectory = "", bool systemHeader = false);
		void loadFromString(std::string const& source);
		void loadFromVector(std::vector <char> const& data);

		std::string getFileName() const;
		std::string getDirectory() const;

		void saveToFile(std::string const& fileName);
		explicit operator std::string() const;
		explicit operator std::vector <char>() const;
		explicit operator std::vector <std::string>() const;
	};
}

#endif