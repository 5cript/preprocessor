#include <iostream>

#include "Preprocessor.hpp"
#include "Project.hpp"

using namespace SeeScript;
using namespace SeeScript::Preprocessor;

int main()
{
    SeeScriptProject proj (".", ".");

    // project directory
	proj.AddIncludeDirectory("G:/Development/CodeBlocks/Preprocessor");

	// for boost:
	proj.AddIncludeDirectory("G:/msys64/mingw64/include");

	// a source to preprocess
    SourceFile source(&proj, "./Sources/main.c");

    SeeScriptPreprocessor prepro (&proj, &source, true, 256);

    try {
        // preprocesses the source file
		prepro.preprocess();

		// print resulting tokens to cout
		prepro.debugPrint();

		// print warnings
		std::cout << "\nWarnings:\n";
		for (auto const& i : prepro.getWarnings())
		{
			std::cout << i.what() << " in " << i.file() << " at line " << i.line() << ": " << i.message() << "\n";
		}
	}
	catch (Error& exc)
	{
	    // an error occured? print them
		auto& i = exc;
		std::cout << i.what() << " in " << i.file() << " at line " << i.line()+1 << ": " << i.message() << "\n\n";
		prepro.debugPrint();
	}

    return 0;
}
