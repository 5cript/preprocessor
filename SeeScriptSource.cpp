#include "SeeScriptSource.hpp"
#include "Project.hpp"

#include <fstream>
#include <boost/filesystem.hpp>

namespace SeeScript {
	SourceFile::SourceFile(SeeScriptProject const* const project, std::string const& fileName, std::string const& relativeSearchDirectory, bool systemHeader)
		: project_(project)
		, fileName_(fileName)
		, content_()
		, systemHeader_(systemHeader)
	{
		if (fileName != "")
			loadFromFile(fileName, relativeSearchDirectory, systemHeader);
	}
	void SourceFile::loadFromFile(std::string const& fileName, std::string const& relativeSearchDirectory, bool systemHeader)
	{
		systemHeader_ = systemHeader;
		fileName_ = project_->findSource(fileName, relativeSearchDirectory, systemHeader);

		std::ifstream file (fileName_, std::ios_base::binary);
		char buffer[512];
		do {
			file.read(buffer, 512);
			content_.insert(content_.end(), std::begin(buffer), std::begin(buffer) + file.gcount());
		} while (file.gcount() == 512);
		normalize();
	}
	void SourceFile::loadFromString(std::string const& source)
	{
		content_.assign(source.begin(), source.end());
		normalize();
	}
	void SourceFile::loadFromVector(std::vector <char> const& data)
	{
		content_ = data;
		normalize();
	}
	std::string SourceFile::getFileName() const
	{
		return fileName_;
	}
	std::string SourceFile::getDirectory() const
	{
		if (fileName_.empty())
			return {};
		
		using boost::filesystem::path;
		return (path{fileName_}).parent_path().string();
	}
	void SourceFile::saveToFile(std::string const& fileName)
	{
		std::ofstream file (fileName, std::ios_base::binary);
		file.exceptions( std::ios::failbit );
		for (auto const& i : content_)
			file.put(i);
	}
	SourceFile::operator std::string() const
	{
		return std::string (content_.begin(), content_.end());		
	}
	SourceFile::operator std::vector <char>() const
	{
		return content_;
	}
	SourceFile::operator std::vector <std::string>() const 
	{
		std::vector <std::string> result;
		std::string tempLine;
		for (auto const& i : content_) {
			if (i == '\n') {
				result.push_back(tempLine);
				tempLine.clear();
			}
			else
				tempLine.push_back(i);
		}
		result.push_back(tempLine);
		return result;
	}
	void SourceFile::normalize() 
	{
		std::vector <char> temp;
		for (auto i = std::begin(content_); i != std::end(content_); ++i) {
			switch (*i) {
				case ('\r'): // CR
				{
					if (i+1 < std::end(content_) && *(i+1) == '\n')
						++i;
					temp.push_back('\n');
					continue;
				}
				default:
					temp.push_back(*i);
			}
		}	
		content_ = temp;
	}
}