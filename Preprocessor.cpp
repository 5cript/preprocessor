#include "Preprocessor.hpp"

#include "Preprocessor/Helpers.hpp"
#include "Preprocessor/Warnings.hpp"
#include "Preprocessor/Errors.hpp"
#include "Preprocessor/Mathematics.hpp"
#include "Preprocessor/Constants.hpp"

#include "FindFirstOf.hpp"
#include "SeeScriptSource.hpp"

#include <stdexcept>
#include <algorithm>
#include <deque>
#include <sstream>
#include <iterator>
#include <iostream>
#include <utility>

#include <boost/algorithm/string.hpp>

// FATAL currently is nothing else than ERROR, but in case error recovery gets added, this will ensure
// that functions that cannot recover from errors use FATAL to bail out
#define ERROR_DIRECTIVE(MSG, LINE) throw Error(MSG, 0, LINE, pImpl->source_->getFileName())
#define FATAL(TYPE, LINE) throw TYPE(LINE, pImpl->source_->getFileName())
#define ERROR(TYPE, LINE) throw TYPE(LINE, pImpl->source_->getFileName())
#define WARN(TYPE, LINE) pImpl->warnings_.emplace_back(TYPE(LINE, pImpl->source_->getFileName()))

#define FATAL_ADD(TYPE, LINE, ADDITION) throw TYPE(LINE, pImpl->source_->getFileName(), std::vector<std::string>ADDITION)
#define ERROR_ADD(TYPE, LINE, ADDITION) throw TYPE(LINE, pImpl->source_->getFileName(), std::vector<std::string>ADDITION)
#define WARN_ADD(TYPE, LINE, ADDITION) pImpl->warnings_.emplace_back(TYPE(LINE, pImpl->source_->getFileName(), std::vector<std::string>ADDITION))

#define LINE_NUMBER std::distance(std::begin(pImpl->tokens_), line)

namespace SeeScript { namespace Preprocessor {

	SeeScriptPreprocessorImpl::SeeScriptPreprocessorImpl(SeeScriptProject const* const project,
														 SourceFile const* const source,
														 bool trigraphs,
														 unsigned int inclusionDepthMax,
														 unsigned int inclusionDepth)
		: project_{project}
		, source_{source}
		, trigraphs_{trigraphs}
		, inclusionDepthMax_{inclusionDepthMax}
		, inclusionDepth_{inclusionDepth}
		, tokens_{}
		, lines_{}
		, line_corrections_{}
		, warnings_{}
		, definitions_{}
	{

	}
	SeeScriptPreprocessor::SeeScriptPreprocessor(SeeScriptProject const* const project,
												 SourceFile const* const source,
												 bool trigraphs,
												 unsigned int inclusionDepthMax,
												 unsigned int inclusionDepth)
		: pImpl{new SeeScriptPreprocessorImpl {project, source, trigraphs, inclusionDepthMax, inclusionDepth}}
	{

	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::line_erase(token_iterator& line)
	{
		line->clear();
		++line;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::resetState()
	{
		pImpl->lines_.clear();
		pImpl->line_corrections_.clear();
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::replaceDigraphs()
	{
		// replace digraphs
		for (auto const& graph : digraphs) {
			for (auto& i : pImpl->lines_) {
				boost::algorithm::replace_all(i, graph.first, graph.second);
			}
		}
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::replaceTrigraphs()
	{
		// replace trigraphs:
		/*
		for (auto const& graph : trigraphs) {
			for (auto& i : pImpl->lines_) {
				boost::algorithm::replace_all(i, graph.first, graph.second);
			}
		}
		*/
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::mergeExtendedLines()
	{
		std::deque <std::vector<std::string>::size_type> rem_list;
		for (auto i = std::begin(pImpl->lines_); i != std::end(pImpl->lines_); ++i) {
			boost::algorithm::trim_left(*i);
			auto cur = i + 1;
			int counter = 0;
			for (; cur < std::end(pImpl->lines_) && !i->empty() && i->back() == '\\'; ++cur) {
				i->pop_back();
				i->append(*(cur));
				boost::algorithm::trim_left(*i);
				rem_list.push_back(cur - std::begin(pImpl->lines_));
				pImpl->line_corrections_[i - std::begin(pImpl->lines_)] = ++counter;
			}
			i = cur - 1;
		}

		removeFromList(pImpl->lines_, rem_list);
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::removeComments()
	{
		for (auto i = std::begin(pImpl->lines_); i != std::end(pImpl->lines_); ++i) {

			std::string curLine = *i;
			auto min = curLine.cbegin();
			static const std::vector <std::string> toFind {
				"//",
				"/*",
				"\"",
				"'"
			};
			FindPair <std::string::const_iterator, std::vector<std::string>::const_iterator> fresult;
			do {
				curLine = std::string(min, curLine.cend());
				fresult = get_first_occurence_of(curLine, toFind);
				min = fresult.where();

				// no comment, nor string
				if (!fresult)
					break;

				// skip string literal
				if (*fresult.which() == "\"") {
					curLine = std::string(min + 1, curLine.cend());
					auto range = boost::algorithm::find_first(curLine, "\"");
					min = range.end();

					if (!range)
						ERROR(IncompleteStringLiteral, std::distance(std::begin(pImpl->lines_), i));
				}

				// skip char literal
				if (*fresult.which() == "'") {
					curLine = std::string(min + 1, curLine.cend());
					auto range = boost::algorithm::find_first(curLine, "'");
					min = range.end();

					if (!range)
						ERROR(IncompleteCharLiteral, std::distance(std::begin(pImpl->lines_), i));
				}
			} while (*fresult.which() == "\"" || *fresult.which() == "'");
			auto tempCurLine = std::string(min, curLine.cend());

			if (!fresult) // no comment
				continue;

			// C++ style comment
			if (*fresult.which() == "//") {
				*i = std::string(std::begin(*i), std::end(*i) - tempCurLine.length()) + " ";
				continue;
			}

			if (*fresult.which() == "/*") { // C style comment
				// search end of multiline comment
				auto cur = i;
				boost::iterator_range <std::string::iterator> posCppEndRange;
				for (; cur != std::end(pImpl->lines_) && !(posCppEndRange = boost::algorithm::find_first(*cur, "*/")); ++cur);

				// only in 1 line
				if (cur == i)
					*i = std::string(curLine.cbegin(), fresult.where()) + " " + std::string(posCppEndRange.end(), std::end(*i));
				// in multiple lines
				else {
					// remove all "only comments" lines
					int del_pos = i - std::begin(pImpl->lines_) + 1;
					int recur = cur - i - 1;
					for (int n = 0; n != recur; ++n) {
						auto erpos = std::begin(pImpl->lines_) + del_pos;
						pImpl->lines_.erase(erpos);
						pImpl->line_corrections_[del_pos]++;
					}
					if (recur)
						cur = i + 1;

					*i = std::string(curLine.cbegin(), fresult.where()) + " ";
					*cur = std::string(boost::algorithm::find_first(*cur, "*/").end(), std::end(*cur));
				}
			}

			curLine = tempCurLine;
		}

		for (auto const& i : pImpl->line_corrections_)
		{
			std::vector <std::string> vec (i.second);
			pImpl->lines_.insert(pImpl->lines_.begin() + i.first, std::begin(vec), std::end(vec));
		}
		pImpl->line_corrections_.clear();
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::initialProcessing()
	{
		// changes internal state
		replaceDigraphs();

		// changes internal state
		replaceTrigraphs();

		// changes internal state
		mergeExtendedLines();

		// replace comments (with single whitespace)
		removeComments();

		// fin
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::tokenize()
	{
		// The preprocessor is greedy, it makes the largest possible token
		// eg:
		// a+++++b is interpreted as a ++ ++ + b

		using namespace Tokenizer;

		for (auto line = std::begin(pImpl->lines_); line != std::end(pImpl->lines_); ++line)
		{
			std::vector <std::unique_ptr <Token>> curLineTokens;
			// traverse lines and extract each type of token
			for (auto c = line->cbegin(); c != line->cend(); ++c)
			{
				if (isWhitespace(*c))
					continue;

				// find applicable tokens:
				Token* token = nullptr;
				if (curLineTokens.size() == 2 && *curLineTokens.back() == "include" && *curLineTokens.front() == "#")
				{
					token = HeaderStringToken::applicable(c, std::end(*line));
					if (token == nullptr)
						ERROR(IncludeErroneous, std::distance(std::begin(pImpl->lines_), line));
				}
				else
				{
					token = extractToken(c, std::end(*line));
					if (dynamic_cast <StringLiteralToken*> (token))
					{
						if (*c != '"')
							ERROR(InvalidStringLiteral, std::distance(std::begin(pImpl->lines_), line));
					}
					else if (dynamic_cast <CharLiteralToken*> (token))
					{
						if (*c != '\'')
							ERROR(InvalidCharLiteral, std::distance(std::begin(pImpl->lines_), line));
					}
				}
				if (token)
					curLineTokens.emplace_back(token);
				else
					curLineTokens.emplace_back(new GenericToken{std::string{*c}});
			}
			pImpl->tokens_.push_back(std::move(curLineTokens));
		}
	}
//---------------------------------------------------------------------------------------------------------------------------------
	SeeScriptPreprocessor::token_iterator SeeScriptPreprocessor::correspondingDirective(token_iterator line, std::string const& which_directive)
	{
		int counter = 1;
		++line;
		for (; line != std::end(pImpl->tokens_); ++line)
		{
			// if the first token is not a #, then skip line
			if (line->size() >= 2 && *line->front() == '#')
			{
				auto directive = *line->operator[](1);
				if (directive == ENDIF_DIRECTIVE)
				{
					--counter;
					if (which_directive == ENDIF_DIRECTIVE)
					{
						if (counter == 0)
							return line;
					}
					else if (counter == 0)
					{
						break; // breaks and returs end
					}
				}
				else if (	directive == IFDEF_DIRECTIVE
						 || directive == IFNDEF_DIRECTIVE
						 || directive == IF_DIRECTIVE)
				{
					++counter;
				}
				// checking for counter == 1 here makes sure that the else(elif) is really the correct corresponding one.
				// (and not an else(elif) of a nested conditional)
				if (counter == 1)
				{
					if (	(directive == ELSE_DIRECTIVE && which_directive == ELSE_DIRECTIVE)
						||	(directive == ELIF_DIRECTIVE && which_directive == ELIF_DIRECTIVE)	)
					{
						return line;
					}
				}
			}
		}
		return std::end(pImpl->tokens_);
	}
//---------------------------------------------------------------------------------------------------------------------------------
	SeeScriptPreprocessor::token_iterator SeeScriptPreprocessor::correspondingElse(token_iterator line)
	{
		return correspondingDirective(line, ELSE_DIRECTIVE);
	}
//---------------------------------------------------------------------------------------------------------------------------------
	SeeScriptPreprocessor::token_iterator SeeScriptPreprocessor::correspondingElif(token_iterator line)
	{
		return correspondingDirective(line, ELIF_DIRECTIVE);
	}
//---------------------------------------------------------------------------------------------------------------------------------
	SeeScriptPreprocessor::token_iterator SeeScriptPreprocessor::correspondingEndif(token_iterator line)
	{
		return correspondingDirective(line, ENDIF_DIRECTIVE);
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::processDirective(token_iterator line)
	{
		if (line->size() == 1)
		{
			processNulldirective(line);
			return;
		}

		std::string directive_name = *line->operator[](1);
		try {
			// not using map here on purpose
			// was trying to use a hash powered string switch,
			// but due to a conformance defect in VS2013 (GRRRR!!!),
			// this was not impossible to archieve in an endurable and sane approach.
			// (also constexpr support missing -> no variadic template hacking or any of that stuff) :(
			if (directive_name == INCLUDE_DIRECTIVE)
			{
				processInclude(line);
			}
			else if (directive_name == ERROR_DIRECTIVE)
			{
				expandLine(line);
				processError(line);
			}
			else if (directive_name == UNDEFINE_DIRECTIVE)
			{
				if (line->size() == 2)
					ERROR_ADD(MissingMacroName, LINE_NUMBER, {UNDEFINE_DIRECTIVE});
				else
					processUndefine(line);
			}
			else if (directive_name == DEFINE_DIRECTIVE)
			{
				if (line->size() == 2)
					ERROR_ADD(MissingMacroName, LINE_NUMBER, {DEFINE_DIRECTIVE});
				else
					processGenericDefine(line);
			}
			else if (directive_name == IFDEF_DIRECTIVE)
			{
				if (line->size() == 2)
					ERROR_ADD(MissingMacroName, LINE_NUMBER, {IFDEF_DIRECTIVE});
				else
					processDefinitionCheck(line, true);
			}
			else if (directive_name == IFNDEF_DIRECTIVE)
			{
				if (line->size() == 2)
					ERROR_ADD(MissingMacroName, LINE_NUMBER, {IFNDEF_DIRECTIVE});
				else
					processDefinitionCheck(line, false);
			}
			else if (directive_name == ENDIF_DIRECTIVE)
			{
				ERROR(DanglingEndif, LINE_NUMBER);
				return;
			}
			else if (directive_name == IF_DIRECTIVE)
			{
				expandLine(line);
				processIf(line);
			}
			else if (directive_name == ELSE_DIRECTIVE)
			{
				processElse(line);
			}
			else if (directive_name == ELIF_DIRECTIVE)
			{
				debugPrint();
				ERROR(DanglingElif, LINE_NUMBER);
			}
			else
			{
				ERROR(UnknownDirective, LINE_NUMBER);
			}
		}
		catch (CarryThrough&)
		{
			// ignore current directive
		}
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::processGenericIf(token_iterator line, bool enter)
	{
		// find the corresponding endif (or else if existing)
		auto else_pos = correspondingElse(line);
		auto elif_pos = correspondingElif(line);
		bool has_else = else_pos != std::end(pImpl->tokens_); // for later use
		bool has_elif = elif_pos != std::end(pImpl->tokens_);
		auto endif_pos = correspondingEndif(has_else ? else_pos : line);

		// there is no corresponding endif? -> ERROR!
		if (endif_pos == std::end(pImpl->tokens_))
		{
			ERROR_ADD(UnterminatedConditional, LINE_NUMBER, {IFDEF_DIRECTIVE});
			return;
		}

		// remove else branch
		if (enter)
		{
			if (has_elif)
				line = elif_pos;
			else if (has_else)
				line = else_pos;
			else
				line = endif_pos;

			for (;line != endif_pos;)
			{
				line_erase(line);
			}
			// remove endif
			{
				line_erase(line);
			}
		}
		else // or remove head
		{
			++line; // do not remove the if itself -> error in main loop

			auto epos = decltype(line)();

			if (has_elif) epos = elif_pos;
			else if (has_else) epos = else_pos;
			else epos = endif_pos;

 			for (;line != epos;)
			{
				line_erase(line);
			}

			if (has_elif)
				elif_pos->operator[](1).reset(new Tokenizer::IdentifierToken ("if")); // little trick ;)
			else
			{
				if (has_else) {
					line_erase(else_pos);
				}
				line_erase(endif_pos);
			}
		}
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::processDefinitionCheck(token_iterator line, bool check_for_defined)
	{
		// we are guaranteed to have at least 3 tokens if we enter this function
		if (line->size() > 3)
			WARN_ADD(RedundandTokens, LINE_NUMBER, {IFDEF_DIRECTIVE});

		std::string definition_name = *line->operator[](2);
		bool enter = (pImpl->definitions_.find(definition_name) == pImpl->definitions_.end()) != check_for_defined;

		processGenericIf(line, enter);
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::processElse(token_iterator line)
	{
		if (line->size() > 2)
			WARN_ADD(RedundandTokens, LINE_NUMBER, {INCLUDE_DIRECTIVE});

		auto endif_pos = correspondingEndif(line);
		// there is no corresponding endif? -> ERROR!
		if (endif_pos == std::end(pImpl->tokens_))
		{
			ERROR_ADD(UnterminatedConditional, LINE_NUMBER, {ELSE_DIRECTIVE});
			return;
		}

		++line; // do not delete the #else itself -> error in main loop
		for (;line != endif_pos;)
		{
			line_erase(line);
		}
		// erase the endif too
		{
			line_erase(line);;
		}

	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::processElif(token_iterator line)
	{
		return; // noop
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::processNulldirective(token_iterator line)
	{
		return; // noop
	}
//---------------------------------------------------------------------------------------------------------------------------------
	int SeeScriptPreprocessor::fetchMacroParameters(token_iterator line, Definition& definition)
	{
		auto text_line = LINE_NUMBER;

		definition.hasArgumentList = true;

		// get all parameters:
		std::size_t i = 4;
		for (; i < line->size() && *line->operator[](i) != ')'; ++i)
		{
			std::string token = *line->operator[](i);
			if (token == "(")
			{
				ERROR(UnallowedParanthesis, text_line);
				return i;
			}

			// parameters may only consist of identifier tokens
			if (token == "...")
			{
				// is variadic macro now
				// peek next token (must be closing paranthesis)
				++i;
				if (i == line->size() || *line->operator[](i) != ')')
				{
					// expected paranthesis somewhere in macro definition
					ERROR(MissingParanthesis, text_line);
					return i;
				}
				--i;
			}
			else if (dynamic_cast <Tokenizer::IdentifierToken*> (line->operator[](i).get()) == nullptr)
			{
				ERROR_ADD(InvalidTokenInParameters, text_line, {token});
				return i;
			}

			// peek next token (comma separated list or end of parameter list)
			++i;
			if (i == line->size())
			{
				// expected paranthesis somewhere in macro definition
				ERROR(MissingParanthesis, text_line);
				return i;
			}
			if (*line->operator[](i) != ')' && *line->operator[](i) != ',')
			{
				ERROR(MustBeCommaSeparated, text_line);
				return i;
			}
			else
			{
				if (std::find(std::begin(definition.parameters), std::end(definition.parameters), token) != std::end(definition.parameters))
				{
					ERROR(DuplicateParameter, text_line);
					return i;
				}
				definition.parameters.push_back(token);
			}

			// end of list
			if (*line->operator[](i) == ')')
			{
				break;
			}
		}

		if (i == line->size())
		{
			// expected paranthesis somewhere in macro definition
			ERROR(MissingParanthesis, text_line);
			return i;
		}

		return i;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::processGenericDefine(token_iterator line)
	{
		auto text_line = LINE_NUMBER;
		auto& text = pImpl->lines_[text_line];
		// auto def_pos = text.find(DEFINE_DIRECTIVE);

		Definition definition;
		std::size_t next_token = 3;
		if (line->size() > 3 && *line->operator[](3) == '(')
		{
			// find paranthesis
			auto pos = text.find('(');
			if (pos == std::string::npos)
				FATAL_ADD(CriticalIPE, text_line, {std::string{"first paranthesis went missing"}});

			if (!isWhitespace(text[pos - 1])) // cannot be out of range, because we got here (there must at least be a # operator)
			{
				// fetches macro params and stores them in the definition class.
				auto forward = fetchMacroParameters(line, definition);

				next_token = forward + 1;
			}
			// else: no parameters in macro
		}

		// grab tokens
		for (auto i = next_token; i < line->size(); ++i)
		{
			definition.tokens.emplace_back(line->operator[](i)->clone());
		}

		// def already known -> redefinition!
		if (pImpl->definitions_.find(*line->operator[](2)) != pImpl->definitions_.end())
			WARN_ADD(Redefinition, text_line, {*line->operator[](2)});

		// add definition to list
		pImpl->definitions_[*line->operator[](2)] = definition;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::processUndefine(token_iterator line)
	{
		if (line->size() != 3)
			WARN_ADD(RedundandTokens, LINE_NUMBER, {UNDEFINE_DIRECTIVE});

		std::string defName = *line->operator[](2);
		auto iter = pImpl->definitions_.find(defName);
		if (iter != std::end(pImpl->definitions_))
		{
			pImpl->definitions_.erase(iter);
		}
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::processError(token_iterator line)
	{
		std::string token_cat = {};
		for (std::size_t i = 2; i != line->size(); ++i)
		{
			token_cat += *line->operator[](i);
			token_cat.push_back(' ');
		}
		ERROR_DIRECTIVE(token_cat, LINE_NUMBER);
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::processInclude(token_iterator line)
	{
		if (line->size() != 3)
			WARN_ADD(RedundandTokens, LINE_NUMBER, {INCLUDE_DIRECTIVE});

		if (line->size() == 2)
			ERROR(IncludeErroneous, LINE_NUMBER);

		if (pImpl->inclusionDepth_ + 1 >= pImpl->inclusionDepthMax_)
			FATAL(IncludeNested, LINE_NUMBER);

		try {
			SourceFile includeFile {pImpl->project_,
									*line->operator[](2),
									pImpl->source_->getDirectory(),
									static_cast <Tokenizer::HeaderStringToken*> (&*line->operator[](2))->isSystemHeader()};
			SeeScriptPreprocessor preprocessor {
				pImpl->project_,
				&includeFile,
				pImpl->trigraphs_,
				pImpl->inclusionDepthMax_,
				++pImpl->inclusionDepth_
			};
			preprocessor.preprocess(pImpl->definitions_);
			pImpl->tokens_.insert(++line, std::make_move_iterator(std::begin(preprocessor.pImpl->tokens_)),
										  std::make_move_iterator(std::end(preprocessor.pImpl->tokens_))	);

			for (auto const& def : preprocessor.pImpl->definitions_)
				pImpl->definitions_[def.first] = def.second;
		}
		catch (file_not_found_exception&)
		{
			ERROR(IncludeFileNotFound, LINE_NUMBER);
		}
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::processIf(token_iterator line)
	{
		int result = 0;
		evaluateConditionalExpression(line, result);
		processGenericIf(line, result != 0);
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::expandLine(token_iterator& line)
	{
		std::vector <std::string> alreadyExpanded;
		expandTokensFully(*line, line, alreadyExpanded);
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void
	SeeScriptPreprocessor::gatherArguments(std::vector <std::unique_ptr <Tokenizer::Token>>::iterator& titer,
										   std::vector <std::unique_ptr <Tokenizer::Token>>::iterator const& end,
										   std::vector <std::vector <std::unique_ptr <Tokenizer::Token>>>& actualParameters,
										   token_iterator line)
	{
	    /**
	     *  FIXME: THIS IS BROKEN FOR MULTILINE ARGUMENTS!
	     */

		int paranthesis_counter = 0;
		do {
			std::vector <std::unique_ptr<Tokenizer::Token>> current;
			for (; titer != end; ++titer)
			{
				auto& aref = *titer;
				if (aref->toString() == "(")
				{
					paranthesis_counter++;
					if (paranthesis_counter == 1)
						continue;
				}
				if (aref->toString() == ")")
				{
					paranthesis_counter--;
					if (paranthesis_counter == 0)
					{
						if (!current.empty())
							actualParameters.push_back(std::move(current));
						return;
					}
				}
				if (aref->toString() == "," && paranthesis_counter == 1)
				{
					// add empty token list too, so that the following macro calls
					// are also valid:
					//
					//	min(,)
					//	min(,b)
					//
					// (this is not an error, its a feature)

					actualParameters.push_back(std::move(current));
					current = decltype(current){}; // reset holder of current parameter token list
				}
				else
				{
					// add current token
					current.emplace_back(aref->clone());
				}
			}
			// once seeked for closing paranthesis in following lines
			/*
			if (paranthesis_counter != 0)
			{
				auto pos = i - std::begin(tokens);
				line++;
				if (line == std::end(pImpl->tokens_))
					FATAL_ADD(UnterminatedArgumentList, LINE_NUMBER, {def->first});
				for (auto const& j : *line)
				{
					// move coming line into this one
					tokens.emplace_back(j->clone());
				}
				i = std::begin(tokens) + pos;
			}
			*/
		} while (paranthesis_counter != 0);

		if (paranthesis_counter != 0)
		{
			ERROR(UnterminatedArgumentList, LINE_NUMBER);
		}

		// once removed empty token lines
		/*
		if (first_line != line)
		{
			*first_line = std::move(*line);
			first_line++;
			for (;first_line != line; ++first_line)
			{
				first_line->clear();
			}
			*line = std::vector <std::unique_ptr<Tokenizer::Token>>{};
		}
		*/
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::expand(token_iterator line, Definition const& definition,
									   std::vector <std::vector <std::unique_ptr<Tokenizer::Token>>> const& actualParameters,
									   bool stringizeNext, std::vector <std::unique_ptr <Tokenizer::Token>>& newTokens,
									   std::vector <std::unique_ptr <Tokenizer::Token> >& temporaryBranch)
	{
		for (auto const& j : definition.tokens)
		{
			auto tokenString = j->toString();

			if (tokenString == "#")
			{
				stringizeNext = true;
				continue;
			}

			if (tokenString != "__VA_ARGS__" && tokenString != "...")
			{
				// is token of definition a parameter?
				auto paramIter = std::find(std::begin(definition.parameters), std::end(definition.parameters), tokenString);

				// no it is not
				if (paramIter == std::end(definition.parameters))
				{
					if (stringizeNext)
						ERROR(StringizeNonParam, LINE_NUMBER);
					temporaryBranch.emplace_back(j->clone());
				}

				// yes it is:
				else
				{
					// get relative formal parameter number:
					std::size_t num = (paramIter - std::begin(definition.parameters));
					if (num >= actualParameters.size())
						FATAL_ADD(CriticalIPE, LINE_NUMBER, {"Error Report failed - parameter count mismatch"});

					// get corresponding actual parameter
					if (!stringizeNext)
					{
						for (auto const& parameterFromPack : actualParameters[num])
						{
							temporaryBranch.emplace_back(parameterFromPack->clone());
						}
					}
					else
					{
						std::vector <std::unique_ptr <Tokenizer::Token> > temp;
						for (auto const& parameterFromPack : actualParameters[num])
						{
							temp.emplace_back(parameterFromPack->clone());
						}
						temporaryBranch.emplace_back(stringize(temp, line));
						stringizeNext = false;
					}
				}
			}
			else if (tokenString == "__VA_ARGS__")
			{
				auto ellipsis_iter = std::find(std::begin(definition.parameters), std::end(definition.parameters), "...");
				if (ellipsis_iter == std::end(definition.parameters))
				{
					WARN(VariadicMacroVA_ARGS, LINE_NUMBER);
					newTokens.emplace_back(j->clone());
				}
				else
				{
					auto numberOfActuals = ellipsis_iter - std::begin(definition.parameters);

					if (!stringizeNext)
					{
						for (auto iter = std::begin(actualParameters) + numberOfActuals;
								  iter != std::end(actualParameters); ++iter)
						{
							for (auto const& parameterFromPack : *iter)
							{
								temporaryBranch.emplace_back(parameterFromPack->clone());
							}
							if ((iter + 1) < std::end(actualParameters))
								temporaryBranch.emplace_back(new Tokenizer::OperatorToken(","));
						}
					}
					else
					{
						std::vector <std::unique_ptr <Tokenizer::Token> > temp;
						for (auto iter = std::begin(actualParameters) + numberOfActuals;
								iter != std::end(actualParameters); ++iter)
						{
							for (auto const& parameterFromPack : *iter)
							{
								temp.emplace_back(parameterFromPack->clone());
							}
						}
						temporaryBranch.emplace_back(stringize(temp, line));
						stringizeNext = false;
					}
				}
			}
		}
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::expandTokensFully(std::vector <std::unique_ptr <Tokenizer::Token>>& tokens, token_iterator line,
												  std::vector <std::string> alreadyExpanded)
	{
		std::vector <std::unique_ptr <Tokenizer::Token>> newTokens; // this will hold all results
		bool stringizeNext = false;

		//###############################################  LAMBDAS  ###################################################################

		// lambda that tests the next token after i for "test_for"
		auto test_next = [&](std::string const& test_for, std::remove_reference<decltype(tokens)>::type::iterator i) {
			return (i + 1 < std::end(tokens) && (*(i + 1))->toString() == test_for);
		};

		static auto moveTokensOver = [](std::remove_reference<decltype(tokens)>::type& destination,
										std::remove_reference<decltype(tokens)>::type& source) {
			destination.insert(destination.end(), std::make_move_iterator(std::begin(source)),
												  std::make_move_iterator(std::end(source)) );
		};

		auto addToken = [&](std::remove_reference<decltype(tokens)>::type::iterator const& iter) {
			if (stringizeNext)
			{
				std::vector <std::unique_ptr <Tokenizer::Token>> toks;
				toks.emplace_back(std::move(*iter));
				newTokens.emplace_back (
					stringize(toks, line)
				);
				stringizeNext = false;
			}
			else
				newTokens.emplace_back(std::move(*iter));
		};

		//################################################################################################################################

		// there is a token pasting operator in an invalid position
		if (!tokens.empty() && *tokens.front() == "##")
			ERROR(TokenPastingOperatorMisplaced, LINE_NUMBER);

		// iterate over all tokens in the passed token list
		auto i = std::begin(tokens);
		if (tokens.size() >= 2 && alreadyExpanded.empty())
		{
			if ((*i)->toString() == "#")
			{
				addToken(std::begin(tokens));
				addToken(std::begin(tokens) + 1);
				i += 2;
			}
		}

		// traverse all tokens
		for (; i != std::end(tokens); ++i)
		{
			// token pasting / token concatenation
			if (test_next("##", i))
			{
				if (i + 2 == std::end(tokens))
				{
					ERROR(TokenPastingOperatorMisplaced, LINE_NUMBER);
				}
				auto tok = Tokenizer::tokenConcatentator((*i)->toString(), (*(i+2))->toString());
				if (tok == nullptr)
				{
					std::vector <std::string> tvec {(*i)->toString(), (*(i+2))->toString()};
					ERROR_ADD(InvalidTokenCombination, LINE_NUMBER, (std::move(tvec)));
				}
				else
				{
					auto offset = i - std::begin(tokens);
					std::remove_reference<decltype(tokens)>::type temp;
					temp.insert(temp.end(), std::make_move_iterator(std::begin(tokens)), std::make_move_iterator(i));
					temp.emplace_back(tok);
					temp.insert(temp.end(), std::make_move_iterator(i+3), std::make_move_iterator(std::end(tokens)));
					tokens = std::move(temp);
					i = std::begin(tokens);
					std::advance(i, offset);
				}
			}

			if ((*i)->toString() == "#")
			{
				ERROR_ADD(StrayPPOP, LINE_NUMBER, {"#"});
			}

			if ((*i)->getID() == Tokenizer::TokenID::IDENTIFIER)
			{
				// is identifier a macro?
				auto def = pImpl->definitions_.find((*i)->toString());
				if (def == pImpl->definitions_.end())
				{
					// the identifier is not a macro, so just add the identifier to the token list
					addToken(i);
					continue;
				}

				// self referential macro breaker
				//####################################################################################
				if (std::find(std::begin(alreadyExpanded), std::end(alreadyExpanded), def->first)
					!= std::end(alreadyExpanded))
				{
					addToken(i);
					continue;
				}
				//####################################################################################

				// amount of expected parameters:
				auto expectedCount = def->second.parameters.size();

				// if expectedCount > 0 -> functionMacro
				if (def->second.hasArgumentList)
				{
					if (!test_next("(", i))
					{
						// is not this macro! -> therefor just an ordinary identifier
						addToken(i);
					}
					else
					{
						++i;

						// get macro arguments
						std::vector <std::vector <std::unique_ptr<Tokenizer::Token>>> actualParameters;
						gatherArguments(i, std::end(tokens), actualParameters, line);

						if (def->second.parameters.empty() && !actualParameters.empty())
						{
							ERROR_ADD(TooMuchActualParameters, LINE_NUMBER,
									  ({def->first, std::to_string(actualParameters.size()), std::to_string(expectedCount)}));
						}

						if (!def->second.parameters.empty())
						{
							// do argument prescan
							argumentPrescan (actualParameters, line);

							// not enough parameters
							if (actualParameters.size() < (expectedCount - (def->second.parameters.back() == "..." ? 1 : 0)))
							{
								ERROR_ADD(NotEnoughActualParameters, LINE_NUMBER,
										  ({def->first, std::to_string(expectedCount), std::to_string(actualParameters.size())}));
							}

							// too much parameters
							else if (actualParameters.size() > expectedCount && def->second.parameters.back() != "...")
							{
								ERROR_ADD(TooMuchActualParameters, LINE_NUMBER,
										  ({def->first, std::to_string(actualParameters.size()), std::to_string(expectedCount)}));
							}
						}

						// DO EXPANSION
						std::vector <std::unique_ptr <Tokenizer::Token> > temporaryBranch;
						expand(line, def->second, actualParameters, stringizeNext, newTokens, temporaryBranch);

						auto alt_state = alreadyExpanded;
						alreadyExpanded.push_back(def->first);
						expandTokensFully(temporaryBranch, line, alreadyExpanded);
						moveTokensOver(newTokens, temporaryBranch);
						alreadyExpanded = std::move(alt_state);
					}
				}
				else // no parameters expected
				{
					std::vector <std::unique_ptr <Tokenizer::Token> > temporaryBranch;
					for (auto const& def_token : def->second.tokens)
					{
						temporaryBranch.emplace_back(def_token->clone());
					}

					auto alt_state = alreadyExpanded;
					alreadyExpanded.push_back(def->first);
					expandTokensFully(temporaryBranch, line, alreadyExpanded);
					moveTokensOver(newTokens, temporaryBranch);
					alreadyExpanded = std::move(alt_state);
				}
			}
			else // is any other token
			{
				newTokens.emplace_back((*i)->clone());
			}
		}
		tokens = std::move(newTokens);

		for (auto const& i : tokens)
		{
			std::cout << i->toString() << " ";
		}
		std::cout << "\n";
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::argumentPrescan (
		std::vector <std::vector <std::unique_ptr <Tokenizer::Token>>>& arguments,
		token_iterator line
	)
	{
		for (auto i = std::begin(arguments); i != std::end(arguments); ++i)
		{
			expandTokensFully(*i, line, {});
		}
	}
//---------------------------------------------------------------------------------------------------------------------------------
	Tokenizer::StringLiteralToken* SeeScriptPreprocessor::stringize(std::vector <std::unique_ptr <Tokenizer::Token>> const& tokens,
																	token_iterator line)
	{
		std::string token { "\"" };
		for (auto const& i : tokens)
		{
			if (i->getID() == Tokenizer::TokenID::STRING_LITERAL)
			{
				token.push_back('\\');
				for (auto const chr : i->toString())
				{
					if (chr == '\\')
						token.push_back('\\');
					token.push_back(chr);
				}
				token.push_back('\\');
			}
			else if (i->getID() == Tokenizer::TokenID::CHAR_LITERAL)
			{
				for (auto const chr : i->toString())
				{
					if (chr == '\\')
						token.push_back('\\');
					token.push_back(chr);
				}
			}
			else
			{
				token += i->toString();
			}
			token.push_back(' ');
		}
		token.pop_back();
		token.push_back('"');
		return new Tokenizer::StringLiteralToken(token);
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::evaluateConditionalExpression(token_iterator line, int& result)
	{
		// 0th: macro expansion
		// first: replace character constants with numerals
		// second: replace defined operations with values
		// third: replace remaining identifiers with 0
		// fourth: parse addition, subtraction, multiplication, division, bitwise operations, shifts, comparisons, and logical operations (&& and ||)

		if (line->size() <= 2)
		{
			ERROR_ADD(MissingExpression, LINE_NUMBER, {IF_DIRECTIVE});
		}

		using namespace Tokenizer;

		std::vector <std::shared_ptr <Token>> line_tokens;
		// iterate over tokens and do replacements:
		{
			auto i = std::begin(*line);
			std::advance(i, 2);
			for (; i != std::end(*line); ++i)
			{
				Token* currentToken = nullptr;
				switch ((*i)->getID())
				{
					case (TokenID::CHAR_LITERAL):
					{
						currentToken = new NumericToken {std::to_string(static_cast <CharLiteralToken*> (i->get())->toNumber())};
						break;
					}
					case (TokenID::IDENTIFIER):
					{
						if (**i == "defined")
						{
							bool isDef = false;
							invokeDefinedOperator(i, line, isDef);
							currentToken = new NumericToken (
								std::to_string(static_cast <int> (isDef))
							);
						}
						else
						{
							currentToken = new NumericToken {"0"};
						}
						break;
					}
					case (TokenID::NUMERAL):
					{
						currentToken = new NumericToken((*i)->toString());
						break;
					}
					case (TokenID::OPERATOR):
					{
						currentToken = new OperatorToken((*i)->toString());
						break;
					}
					default:
						break;
				}
				line_tokens.emplace_back(currentToken);
			}
		} // artificial scope
		// Resolve unaries first:
		{
			for (auto i = std::begin(line_tokens); i != std::end(line_tokens);)
			{
				for (; i != std::end(line_tokens) &&
					// is unary condition:
					   !((*i)->getID() == TokenID::OPERATOR &&
						(  (*i)->toString() == "-" || (*i)->toString() == "+"
						|| (*i)->toString() == "~" || (*i)->toString() == "!" ) &&
						[&]{ return (i == std::begin(line_tokens) || ((*(i-1))->getID() != TokenID::NUMERAL && (*(i-1))->toString() != ")")); }())
					 ; ++i);
				if (i != std::end(line_tokens))
				{
					auto opera = (*i)->toString();
					int offset = i - std::begin(line_tokens);
					line_tokens.erase(i);
					i = std::begin(line_tokens) + (offset);
					std::vector <std::shared_ptr <Token>> range;
					if (opera == "-")
					{
						range = {
							std::shared_ptr<Token>(new OperatorToken{"("}),
							std::shared_ptr<Token>(new NumericToken{"1", true}),
							std::shared_ptr<Token>(new OperatorToken{")"}),
							std::shared_ptr<Token>(new OperatorToken{"*"})
						};
					}
					else if (opera == "!" || opera == "~")
					{
						// !a becomes 0!a
						range = {
							std::shared_ptr<Token>(new NumericToken{"0"}),
							std::shared_ptr<Token>(new OperatorToken{opera})
						};
					}
					/*
						ignore plusses
					*/
					line_tokens.insert(i, std::begin(range), std::end(range));
					i = std::begin(line_tokens);
				}
			}
		} // artificial scope

		/*
		for (auto const& i : line_tokens)
		{
			std::cout << i->toString() << " ";
		}
		std::cout << "\n";
		*/

		// SHUNTING-YARD + RPN_PARSER
		using namespace Mathematics;
		try
		{
			auto rpn = ShuntingYard(line_tokens);
			/*
			for (auto const& i : rpn.output)
			{
				std::cout << i->toString() << " ";
			}
			*/
			result = EvaluateReversePolish(rpn) ? 1 : 0;
		}
		catch (MathematicalError& exc)
		{
			std::cout << '\n' << (int)exc.getInfo().code << ": " << exc.getInfo().additionalInformation << '\n';
			ERROR_ADD(IfMathError, LINE_NUMBER, {exc.getInfo().additionalInformation});
			result = 0;
		}
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::invokeDefinedOperator(std::vector <std::unique_ptr <Tokenizer::Token>>::iterator& iter,
													  token_iterator line,
													  bool& isDefined)
	{
		if ((iter + 1) == std::end(*line))
			ERROR(DefinedOperatorRequiresIdentifier, LINE_NUMBER);
		if (**(iter + 1) == "(")
		{
			if ((iter + 3) == std::end(*line) || **(iter + 3) != ")")
				ERROR(MissingParanthesisAfterDefined, LINE_NUMBER);
			else
			{
				isDefined = pImpl->definitions_.find(static_cast <std::string> (**(iter + 2))) != std::end(pImpl->definitions_);
				std::advance(iter, 3);
				return;
			}
		}
		else if ((*(iter+1))->getID() == Tokenizer::TokenID::IDENTIFIER)
		{
			isDefined = pImpl->definitions_.find(static_cast <std::string> (**(iter + 1))) != std::end(pImpl->definitions_);
			++iter;
			return;
		}
		else
		{
			ERROR(MissingParanthesisAfterDefined, LINE_NUMBER);
		}
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::parseLanguage()
	{
		for (auto line = std::begin(pImpl->tokens_); line != std::end(pImpl->tokens_); )
		{
			if (line->empty())
			{
				++line;
				continue;
			}

			// if the first token is not a #, then skip line
			if (*line->front() == '#')
			{
				// does selective expansion:
				processDirective(line);

				line_erase(line);
			}
			else
			{
				// expand
				expandLine(line);
				++line;
			}
		}
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::removeEmptyTokenLists()
	{
		// gather lines to remove:
		std::deque <std::vector<std::string>::size_type> rem_list;
		int counter = 0;
		int last_empty = 0;
		int last_non_empty_line = 1;
		for (auto const& i : pImpl->tokens_)
		{
			if (i.empty())
			{
				if (last_empty != counter - 1)
					last_non_empty_line = counter;
				pImpl->line_corrections_[last_non_empty_line]++;
				rem_list.push_back(counter);
				last_empty = counter;
			}
			++counter;
		}

		// remove lines
		removeFromList(pImpl->tokens_, rem_list);
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::debugPrint ()
	{
		/*
		std::cout << "\nText\n------------------------------------------------------\n";
		for (auto const& i : pImpl->lines_)
		{
			std::cout << i << "\n";
		}
		std::cout << "------------------------------------------------------\n\n";
		*/
		removeEmptyTokenLists();

		std::cout << "Tokens\n------------------------------------------------------\n";
		for (auto const& i : pImpl->tokens_)
		{
			for (auto const& j : i)
			{
				std::cout << j->toString() << ' ';
			}
			std::cout << "\n";
		}
		std::cout << "------------------------------------------------------\n";

		/*
		std::cout << "Definitions\n------------------------------------------------------\n";
		for (auto const& i : pImpl->definitions_)
		{
			std::cout << i.first << ":\n";
			for (auto const& j : i.second.tokens)
			{
				std::cout << j->toString() << "\t";
			}
			std::cout << "\n\n";
		}
		std::cout << "------------------------------------------------------\n";
		*/
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::replaceEscapeSequences()
	{
		// \a \n \t etc has to be replaced accordingly
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::concatenateStrings()
	{
		// concatenate string literals togehter
	}
//---------------------------------------------------------------------------------------------------------------------------------
	void SeeScriptPreprocessor::preprocess(std::map <std::string, Definition> Predefined)
	{
		pImpl->definitions_ = std::move(Predefined);
		resetState();
		pImpl->lines_ = static_cast <std::vector <std::string> > (*pImpl->source_);

		// Comments, merging, trigraphs
		initialProcessing();
		tokenize();
		parseLanguage();
		removeEmptyTokenLists();
		concatenateStrings();
	}
//---------------------------------------------------------------------------------------------------------------------------------
	std::vector <Warning> SeeScriptPreprocessor::getWarnings() const
	{
		return pImpl->warnings_;
	}
//---------------------------------------------------------------------------------------------------------------------------------
	std::map <std::string, Definition> SeeScriptPreprocessor::getDefaultPredefs(int c_version)
	{
		return {
			{"__SEE_C__", makeDefinition({}, {"0000"}) },
			{"__STDC_VERSION__", makeDefinition({}, {"201112L"}) }
		};
	}
//---------------------------------------------------------------------------------------------------------------------------------
	Definition makeDefinition(std::vector<std::string> const& parameters,
				 			  std::vector<std::string> const& str_tokens)
	{
		std::vector <std::shared_ptr <Tokenizer::Token>> tokens;
		for (auto const& i : str_tokens)
		{
		    auto beg = std::begin(i);
			auto* tok = Tokenizer::extractToken(beg, std::end(i));
			if (tok != nullptr)
				tokens.emplace_back(tok);
		}
		return Definition {
			!parameters.empty(),
			parameters,
			tokens
		};
	}
//---------------------------------------------------------------------------------------------------------------------------------

} // Preprocessor
} // SeeScript
