#include <string>
#include <vector>

template <typename FindIteratorT, typename ListIteratorT>
class FindPair
{
public:
    explicit operator bool() const
    {
        return found_;
    }
    FindIteratorT where() const
    {
        return where_;
    }
    ListIteratorT which() const
    {
        return which_;
    }
    FindPair(FindIteratorT where, ListIteratorT which, bool found = false)
        : where_(std::move(where)), which_(std::move(which)), found_(found)
    {}
	FindPair()
		: where_(), which_(), found_(false)
	{}
private:
    FindIteratorT where_;
    ListIteratorT which_;
    bool found_;
};

template <typename FindIteratorT, typename ListIteratorT>
FindPair <FindIteratorT, ListIteratorT> get_first_occurence_of (FindIteratorT Fbegin, FindIteratorT Fend, ListIteratorT Lbegin, ListIteratorT Lend)
{
    for (auto i = Fbegin; i != Fend; ++i)
    {
        for (auto j = Lbegin; j != Lend; ++j)
        {
            auto before = i;
            bool found = true;
            for (auto k = std::begin(*j); k != std::end(*j); ++k)
            {
                if (*k != *i)
                {
                    found = false;
                    break;
                }
                if (i != Fend)
                    ++i;
                else
                {
                    found = false;
                    break;
                }
            }
            if (found)
            {
				std::advance(i, -std::distance(std::begin(*j), std::end(*j)));
                return {i, j, true};
            }
            else
            {
                i = before;
            }
        }
    }
    return { Fend, Lend, false };
}

template <template <typename ElemT, typename = std::allocator<ElemT>> class ContainerT = std::vector>
inline FindPair <std::string::const_iterator, std::vector<std::string>::const_iterator> get_first_occurence_of (std::string const& str, ContainerT <std::string> const& alts)
{
    return get_first_occurence_of (str.cbegin(), str.cend(), alts.cbegin(), alts.cend());
}