#include "Project.hpp"
#include <exception>

namespace SeeScript {
	std::string normalize_path (std::string const& path)
	{
		return path + ((path.back() == '/' || path.back() == '\\')?(""):("/"));
	}
	const char* invalid_directory_exception::what() const throw()
	{
		return "invalid directory / specified path is not a directory";
	}
	const char* file_not_found_exception::what() const throw()
	{
		return "file not found";
	}

	SeeScriptProject::SeeScriptProject(std::string systemHeaderDirectory, std::string systemLibraryDirectory) 
		: systemHeaderDirectory_{normalize_path(systemHeaderDirectory)}
		, systemLibraryDirectory_{normalize_path(systemLibraryDirectory)}
		, sources_{}
		, compilerIncludeDirectories_ {}
		, linkerLibraryDirectories_ {}
	{
		compilerIncludeDirectories_.emplace_back();
		compilerIncludeDirectories_.emplace_back("./");
	}
	void SeeScriptProject::AddIncludeDirectory(std::string directoryName)
	{
		directoryName = normalize_path(directoryName);
		boost::filesystem::path p(directoryName);
		if (boost::filesystem::is_directory(p))
			compilerIncludeDirectories_.push_back(directoryName);
		else
			throw invalid_directory_exception();
	}
	void SeeScriptProject::AddLibraryDirectory(std::string directoryName)
	{
		directoryName = normalize_path(directoryName);
		boost::filesystem::path p(directoryName);
		if (boost::filesystem::is_directory(p))
			linkerLibraryDirectories_.push_back(directoryName);
		else
			throw invalid_directory_exception();
	}
	std::string SeeScriptProject::findSource(std::string const& fileName, std::string const& relativeSearchDirectory, bool systemHeader) const
	{
		using boost::filesystem::path;
		if (!systemHeader)
		{
			if (!relativeSearchDirectory.empty())
			{
				path concatenated = normalize_path(relativeSearchDirectory) + fileName;
				if (boost::filesystem::is_regular_file(concatenated))	
					return concatenated.string();
			}

			for (auto const& i : compilerIncludeDirectories_) {
				path concatenated = path(i + fileName);
				if (boost::filesystem::is_regular_file(concatenated))	
					return concatenated.string();
			}
		}
		else
		{
			path concatenated = path(systemHeaderDirectory_ + fileName);
			if (boost::filesystem::is_regular_file(concatenated))	
				return concatenated.string();			
		}
// FIXME:
		if (systemHeader)
			return findSource(fileName, relativeSearchDirectory, false);
		throw file_not_found_exception();
		return {};
	}
	std::string SeeScriptProject::findLibrary(std::string const& fileName, bool systemLibrary) const
	{
		using boost::filesystem::path;
		if (!systemLibrary)
		{
			for (auto const& i : linkerLibraryDirectories_) {
				path concatenated = path(i + fileName);
				if (boost::filesystem::is_regular_file(concatenated))	
					return concatenated.string();
			}
		}
		else
		{
			path concatenated = path(systemLibraryDirectory_ + fileName);
			if (boost::filesystem::is_regular_file(concatenated))	
				return concatenated.string();
		}
		throw file_not_found_exception();
		return {};
	}
	void SeeScriptProject::AddSourceFile(std::string const& fileName)
	{
		sources_.emplace_back(this, fileName);
	}
	void SeeScriptProject::AddSource(std::string const& dataString)
	{
		SourceFile File(this);
		File.loadFromString(dataString);
		sources_.push_back(File);
	}
	void SeeScriptProject::AddSource(std::vector <char> const& data)
	{
		SourceFile File(this);
		File.loadFromVector(data);
		sources_.push_back(File);
	}
}