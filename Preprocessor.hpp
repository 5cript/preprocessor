#include "Project.hpp"
#include "Preprocessor/Messages.hpp"
#include "Preprocessor/Tokens.hpp"

#include <string>
#include <vector>
#include <map>
#include <list>

// http://www.mers.byu.edu/docs/standardC/preproc.html
// http://gcc.gnu.org/onlinedocs/cpp/Tokenization.html
// http://tigcc.ticalc.org/doc/cpp.html
// http://www.cplusplus.com/doc/tutorial/preprocessor/

/*
	All arguments to a macro are completely macro-expanded before they are substituted into the macro body.
	After substitution, the complete text is scanned again for macros to expand, including the arguments.
	This rule may seem strange,
	but it is carefully designed so you need not worry about whether any function call is actually a macro invocation.
	You can run into trouble if you try to be too clever, though. See Argument Prescan, for detailed discussion.

	link:
		http://gcc.gnu.org/onlinedocs/cpp/Argument-Prescan.html#Argument-Prescan
*/

/*
	You can leave macro arguments empty;
	this is not an error to the preprocessor (but many macros will then expand to invalid code).
	You cannot leave out arguments entirely; if a macro takes two arguments,
	there must be exactly one comma at the top level of its argument list.
*/

/*
	Macro parameters appearing inside string literals are not replaced by their corresponding actual arguments.
*/

namespace SeeScript { namespace Preprocessor {

	struct Definition {
		bool hasArgumentList;
		std::vector <std::string> parameters;
		std::vector <std::shared_ptr<Tokenizer::Token>> tokens;

		Definition (decltype(hasArgumentList) hasArgumentList = false, decltype(parameters) parameters = {}, decltype(tokens) tokens = {})
			: hasArgumentList(hasArgumentList)
			, parameters(std::move(parameters))
			, tokens(std::move(tokens))
		{}
	};

	Definition makeDefinition(std::vector<std::string> const& parameters,
				 			  std::vector<std::string> const& str_tokens);

	struct SeeScriptPreprocessorImpl
	{
		SeeScriptProject const* const project_;
		SourceFile const* const source_;
		const bool trigraphs_;
		unsigned int inclusionDepthMax_;
		unsigned int inclusionDepth_;
		std::list <std::vector <std::unique_ptr<Tokenizer::Token>>> tokens_; // seperated into lines
		std::vector <std::string> lines_;
		std::map<int, int> line_corrections_;
		std::vector <Warning> warnings_;
		std::map <std::string /* name / identifier */, Definition> definitions_;

		SeeScriptPreprocessorImpl(SeeScriptProject const* const project,
								  SourceFile const* const source,
								  bool trigraphs,
								  unsigned int inclusionDepthMax,
								  unsigned int inclusionDepth);
	};

	class SeeScriptPreprocessor {

		typedef std::list <std::vector <std::unique_ptr<Tokenizer::Token>>>::iterator token_iterator;

	private:
		// recursion optimization to avoid stackoverflows
		std::unique_ptr <SeeScriptPreprocessorImpl> pImpl; // pointer to implementation idiom
	private:

		/**
		 *	erase with line correction
		 */
		void line_erase(token_iterator& line);

		/**
		 *	Reset internal preprocessor state (done bevore each preprocessing)
		 */
		void resetState();

		/**
		 *	Replaces digraphs.
		 */
		void replaceDigraphs();

		/**
		 *	Replaces trigraphs.
		 */
		void replaceTrigraphs();

		/**
		 *	Merge lines that has been marked with a final slash on the end.
		 */
		void mergeExtendedLines();

		/**
		 *	Removes single and multiline comments.
		 */
		void removeComments();

		/**
		 *	1. input file is read and broken into lines
		 *	2. trigraph replacement
		 *	3. lines with the occurence of the line continutation character are merged
		 */
		void initialProcessing();

		/**
		 *	The lines will be split up into tokens
		 */
		void tokenize();

		/**
		 *	Marko expansion / includes / error / warning etc
		 */
		void parseLanguage();

		/**
		 *	Replace all EscapeSequences (\n \a \v ...) with their respective characters withing literals
		 */
		void replaceEscapeSequences();

		/**
		 *	Concatenate string literals eg:
		 *	"abc" " " "def"
		 *	becomes:
		 *	"abc def"
		 */
		void concatenateStrings();

		/**
		 *	RemoveEmptyTokenLists means: ignore empty lines (they get remove first, so that the parser does not have
		 *	to bother)
		 */
		void removeEmptyTokenLists();

		/**
		 *	processes the directive in the passed line (as first argument)
		 */
		void processDirective(token_iterator line);

		/**
		 *	Fetches all parameters from a macro definition
		 *
		 *	@param line The current line.
		 *	@param Definition A macro definition class that (will) hold all information about the macro.
		 *
		 *	@return Returns the amount of tokens to forward the preprocessor.
		 */
		int fetchMacroParameters(token_iterator line, Definition& definition);

		/**
		 *	define (function style & token definition)
		 */
		void processGenericDefine(token_iterator line);

		/**
		 *	undefine
		 */
		void processUndefine(token_iterator line);

		/**
		 *	a nulldirective has been found
		 */
		void processNulldirective(token_iterator line);

		/**
		 *	include
		 */
		void processInclude(token_iterator line);

		/**
		 *	error
		 */
		void processError(token_iterator line);

		/**
		 *	If defined / If not defined
		 */
		void processDefinitionCheck(token_iterator line, bool check_for_defined);

		/**
		 *	Else skipper.
		 *	This function is a counterpart to all conditionals.
		 *	It removes #else parts if the conditional has been evaluated to true.
		 */
		void processElse(token_iterator line);

		/**
		 *	Combination of else and if
		 */
		void processElif(token_iterator line);

		/**
		 *	If
		 */
		void processIf(token_iterator line);

		/**
		 *	For all if statements (ifdef ifndef if)
		 *
		 *	@param line current line
		 *	@param enter = enter the if?
		 */
		void processGenericIf(token_iterator line, bool enter);

		/**
		 *	evaluates operations that are allowed in #if directives
		 */
		void evaluateConditionalExpression(token_iterator line, int& result);

		/**
		 *	evaluates operations that are allowed in #if directives
		 */
		void invokeDefinedOperator(std::vector <std::unique_ptr <Tokenizer::Token>>::iterator& iter,
								   token_iterator line,
								   bool& isDefined);

		/**
		 *	macro expander for lines. Calles respective functions for macros and functions style macros
		 *
		 *	@param line the current line to expand
		 */
		void expandLine(token_iterator& line);

		/**
		 *	argument collector for function style macros
		 *
		 *	@param line the current line to gather tokens from
		 */

		void
		gatherArguments(std::vector <std::unique_ptr <Tokenizer::Token>>::iterator& titer,
						 std::vector <std::unique_ptr <Tokenizer::Token>>::iterator const& end,
						 std::vector <std::vector <std::unique_ptr <Tokenizer::Token>>>& actualParameters,
						 token_iterator line);

		/**
		 *	expand single.
		 */
		void expand(token_iterator line, Definition const& definition,
					std::vector <std::vector <std::unique_ptr<Tokenizer::Token>>> const& actualParameters,
					bool stringizeNext, std::vector <std::unique_ptr <Tokenizer::Token>>& newTokens,
					std::vector <std::unique_ptr <Tokenizer::Token> >& temporaryBranch);

		/**
		 *	expand token list. tokens must evaluate to a valid expression
		 */
		void expandTokensFully(std::vector <std::unique_ptr <Tokenizer::Token>>& tokens, token_iterator line,
							   std::vector <std::string> alreadyExpanded);

		/**
		 *	perform argument prescan for function style macros
		 *	all arguments are fully expanded and returned
		 *
		 *	@param arguments token lists which represent arguments of a function style macro
		 */
		void argumentPrescan (
			std::vector <std::vector <std::unique_ptr <Tokenizer::Token>>>& arguments,
			token_iterator line
		);

		/**
		 *	finder functions
		 *	(for endif and else)
		 *	this function is used by several preprocessor conditionals
		 */
		token_iterator correspondingDirective(token_iterator line, std::string const& which_directive);
		token_iterator correspondingElse(token_iterator line);
		token_iterator correspondingElif(token_iterator line);
		token_iterator correspondingEndif(token_iterator line);

		/**
		 *	Stringizer
		 */
		Tokenizer::StringLiteralToken* stringize(std::vector <std::unique_ptr <Tokenizer::Token>> const& tokens,
												 token_iterator line);

		/**
		 *	Default predefines
		 */
		static std::map <std::string, Definition> getDefaultPredefs(int c_version = 201112);

	public:
		SeeScriptPreprocessor(SeeScriptProject const* const project,
							  SourceFile const* const source,
							  bool trigraphs = false,
							  unsigned int inclusionDepthMax = 512,
							  unsigned int inclusionDepth = 0);

		void preprocess(std::map <std::string, Definition> Predefined = SeeScriptPreprocessor::getDefaultPredefs());

		std::vector <Warning> getWarnings() const;

		// REMOVEME:
		void debugPrint ();
	};

} // Preprocessor
} // SeeScript
